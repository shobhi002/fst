const express = require('express');
const dataController = require('../../controllers/data.controller');

const router = express.Router();

router
  .route('/PrepareInfo')
  .post(dataController.saveData);

  router
  .route('/GetInfo')
  .get(dataController.getData);

module.exports = router;
