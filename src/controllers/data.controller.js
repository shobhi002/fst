const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { dataService } = require('../services');

const saveData = catchAsync(async (req, res) => {
  console.log("aaaaaa");
  let {url} = req.body;
  const data = await dataService.saveData(url);
  if (!data) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Data not found');
  }
  res.send(data);
});

const getData = catchAsync(async (req, res) => {
  console.log("bbbb");
  const query= req.query;
  
  const data = await dataService.getData(query);
  if (!data) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Data not found');
  }
  res.send(data);
});

module.exports = {
  saveData,
  getData
};
