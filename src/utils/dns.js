const dns = require('dns');


const resolve = hostname =>
 new Promise(resolve => {
  dns.lookup(hostname, err => {
   if (err && err.code === 'ENOTFOUND') {
    resolve(false);
   } else {
    
    getIp(hostname, err => {
      reject (1);
    },
    res=>{
      resolve(res);
    });
   }
  });
 })

 const getIp = hostname =>
 new Promise(resolve => {
  dns.lookup(hostname, err => {
   if (err && err.code === 'ENOTFOUND') {
    resolve(false);
   } 
  },res => {
    console.log("res===",res);
    resolve(res);
  });
 })

// async function getIp (url){
//   console.log('url===', url);

//   dns.resolve4(url, { ttl: true },(err, records) => {
//     console.log('err:', err);

//     console.log('records: %j', records);
//     return { ip: records[0] };
//   }
//   );
// }

module.exports = {
  getIp,
  resolve
}

