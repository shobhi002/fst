const httpStatus = require('http-status');
const Data  = require('../models/data.model');
const ApiError = require('../utils/ApiError');
const puppeteer = require('puppeteer');
const uuid = require('uuid');
const config = require('../config/config');
const DNS = require('../utils/dns')
const screenShotsBasePath = config.screenShotsBasePath ? config.screenShotsBasePath : 'screenshots';

const saveData = async (url) => {
  let data={};
  let {ip} = await DNS.resolve(url);
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto(url);
  const imagePath = uuid.v4()+'.png';
  await page.screenshot({ path:  screenShotsBasePath+'/'+imagePath});

  await browser.close();
  data = new Data({
    url,
    image:imagePath,
    ip:ip,
    createdAt: new Date()

  });
  await data.save()
  return data;
};

const getData = async (query) => {
  let data={};
  let queryKeys = Object.keys(query);
  let whereClause = {};
  if(queryKeys && queryKeys.length){
    whereClause = query;
  }
  data = await Data.find(whereClause)
  return data;
};

module.exports = {
  saveData,
  getData
};
