const mongoose = require('mongoose');

const dataSchema = mongoose.Schema(
  {
    url: {
      type: String,
    },
    image:{
      type: String,
    },
    ip:{
      type: String,
    },
    createdAt:{
      type: Date
    }
  },
  {
    timestamps: true,
  }
);

/**
 * @typedef Data
 */
const Data = mongoose.model('Data', dataSchema);

module.exports = Data;
